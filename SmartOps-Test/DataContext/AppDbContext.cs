﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SmartOps_Test.Model;

namespace SmartOps_Test.DataContext
{
    public class AppDbContext : IdentityDbContext<User, Role, int>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Memo> Memos { get; set; }
        public DbSet<MemoCategory> MemoCategories { get; set; }
        public DbSet<Content> Contents { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>()
                .HasMany(c => c.Memos)
                .WithOne(e => e.User);


            modelBuilder.Entity<MemoCategory>().HasData(
            new MemoCategory
            {
                Id = 1,
                Name = "Request"
            },
            new MemoCategory
            {
                Id = 2,
                Name = "Confirmation"
            },
            new MemoCategory
            {
                Id = 3,
                Name = "Periodic Report"
            },
            new MemoCategory
            {
                Id = 4,
                Name = "Ideas and Suggestion"
            },
            new MemoCategory
             {
                 Id = 5,
                 Name = "Informal Study Results"
             });
        }
    }

}
