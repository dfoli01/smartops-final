﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SmartOps_Test.Migrations
{
    public partial class AddedTTLModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasPaid",
                table: "AspNetUsers",
                type: "tinyint(1)",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PaymentContentId",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TTlReference",
                table: "AspNetUsers",
                type: "longtext",
                nullable: true)
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.AddColumn<DateTime>(
                name: "TtlExpiryTime",
                table: "AspNetUsers",
                type: "datetime(6)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasPaid",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "PaymentContentId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TTlReference",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "TtlExpiryTime",
                table: "AspNetUsers");
        }
    }
}
