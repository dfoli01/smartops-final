﻿using System;
using System.Collections.Generic;

namespace SmartOps_Test.Utility
{
    public class Response<T>
    {
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }
        public T Data { get; set; }


        public Response()
        {

        }

        public Response<T> Create(string StatusCode, string Description, T Data)
        {
            this.StatusCode = StatusCode;
            this.Description = Description;
            Message = GetResponseMessage(StatusCode);
            this.Data = Data;

            return this;
        }

        private string GetResponseMessage(string statusCode)
        {
            var responseMessages = new Dictionary<string, string>
            {
                { ResponseCode.SUCCESS, "Success"},
                { ResponseCode.BadRequest, "Bad Request"},
                { ResponseCode.NotFound, "No record Found"}
            };

            return responseMessages[statusCode];
        }
    }

    public class ResponseCode
    {
        public static string SUCCESS = "00";
        public static string BadRequest = "01";
        public static string LockedOut = "02";
        public static string NotFound = "03 ";
    }
}
