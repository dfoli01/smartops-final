﻿using System;
using System.Threading.Tasks;
using SmartOps_Test.Model;

namespace SmartOps_Test.Service.Abstractions
{
    public interface IEmailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
    }
}
