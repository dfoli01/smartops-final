﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SmartOps_Test.Model;

namespace SmartOps_Test.Service.Abstractions
{
    public interface IFireBaseService
    {
        Task<FileDocument> UploadDocumentAsync(FileDocument document);

        FileDocument DownloadDocument(string path);

        IEnumerable<FileDocument> GetAllDocuments();
    }
}
