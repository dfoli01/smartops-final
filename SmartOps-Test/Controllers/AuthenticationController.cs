﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SmartOps_Test.Model;
using SmartOps_Test.Model.InputModel;
using SmartOps_Test.Model.ViewModel;
using SmartOps_Test.Service.Abstractions;
using SmartOps_Test.Utility;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartOps_Test.Controllers
{
    [ApiController]
    [Route("auth")]
    public class AuthenticationController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly ITokenService _tokenService;
        private readonly JwtSetting _jwtSetting;

        public AuthenticationController(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<Role> roleManager, ITokenService tokenService, IOptions<JwtSetting> jwtSetting)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
            _roleManager = roleManager;
            _jwtSetting = jwtSetting.Value;
        }

        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterInputModel model)
        {
            var response = new Response<UserViewModel>();
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.Username,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    IsAdmin = true
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    if (model.IsAdmin)
                    {
                        await _userManager.AddToRoleAsync(user, "Admin");
                    }
                    else
                    {
                        await _userManager.AddToRoleAsync(user, "Customer");
                    }
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    var token = _tokenService.BuildToken(_jwtSetting.Key, user);

                    var userViewModel = new UserViewModel
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Token = token
                    };
                    return Ok(response.Create(ResponseCode.SUCCESS, "Success", userViewModel));
                }
                return BadRequest(response.Create(ResponseCode.BadRequest, result.Errors.First().Description, null));
            }
            else
            {
                var errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return BadRequest(response.Create(ResponseCode.BadRequest, errors[0], null));
            }
        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginInputModel model)
        {
            var response = new Response<UserViewModel>();
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, 
                // set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Username,
                                   model.Password, model.RememberMe, lockoutOnFailure: true);


                if (result.Succeeded)
                {
                    var user = await _userManager.FindByNameAsync(model.Username);
                    var token = _tokenService.BuildToken(_jwtSetting.Key, user);
                    var userViewModel = new UserViewModel
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Token = token
                    };
                    return Ok(response.Create(ResponseCode.SUCCESS, "Success", userViewModel));
                }

                if (result.IsLockedOut)
                {
                    return Ok(response.Create(ResponseCode.LockedOut, "User is locked out", null));
                }
            }
            else
            {
                var errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return BadRequest(response.Create(ResponseCode.BadRequest, errors[0], null));
            }
            return Ok(response.Create(ResponseCode.BadRequest, "User authentication failed", null));

        }

        [HttpPost("logout")]
        public async Task<IActionResult> LogoutAsync([FromBody] LoginInputModel model)
        {
            await _signInManager.SignOutAsync();
            return Ok("Success");

        }
    }
}
