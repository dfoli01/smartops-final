﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SmartOps_Test.DataContext;
using SmartOps_Test.Model;
using SmartOps_Test.Model.InputModel;
using SmartOps_Test.Model.ViewModel;
using SmartOps_Test.Service.Abstractions;
using SmartOps_Test.Utility;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartOps_Test.Controllers
{
    [Authorize]
    [ApiController]
    [Route("memo")]
    public class MemoController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly IEmailService _mailService;

        public MemoController(AppDbContext context, IMapper mapper, IEmailService mailService)
        {
            _context = context;
            _mapper = mapper;
            _mailService = mailService;
        }

        [HttpGet("customerId")]
        public async Task<IActionResult> GetAllAsync(int customerId)
        {
            var response = new Response<IList<MemoViewModel>>();
            var memos = await _context.Memos.Include(x => x.MemoCategory).Where(m => m.UserId == customerId).OrderByDescending(x => x.Id).ToListAsync();
            if (memos.Count() > 0)
            {
                var memoResponse = _mapper.Map<IList<MemoViewModel>>(memos);
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", memoResponse));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, "memos not found", null));
        }

        [HttpGet("id")]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var response = new Response<Memo>();
            var memo = await _context.Memos.FirstOrDefaultAsync(m => m.Id == id);
            if (memo != null)
            {
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", memo));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, "memo not found", null));
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] MemoInputModel model)
        {
            var response = new Response<string>();
            if (!ModelState.IsValid)
            {
                var errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return BadRequest(response.Create(ResponseCode.BadRequest, errors[0], null));
            }

            var memo = new Memo
            {
                Title = model.Title,
                Content = model.Content,
                UserId = model.CustomerId,
                MemoCategoryId = model.CategoryId
            };

            _ =await _context.Memos.AddAsync(memo);
            _ = await _context.SaveChangesAsync();
            if (memo.Id > 0)
            {
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", "Memo created successfully"));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, "Error", "Memo creation failed"));
        }


        [HttpGet("categories")]
        public async Task<IActionResult> GetMemoCategories()
        {
            var response = new Response<List<MemoCategory>>();
            var memoCategories = await _context.MemoCategories.ToListAsync();
            if (memoCategories.Count() > 0)
            {
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", memoCategories));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, "memo categories not found", null));
        }

        [HttpGet("{id}/send-email")]
        public async Task<IActionResult> SendEmail(int id)
        {
            var response = new Response<object>();
            var memo = await _context.Memos.Include(x => x.User).FirstOrDefaultAsync(x => x.Id == id);
            var mailRequest = new MailRequest
            {
                ToEmail = memo.User.Email,
                Subject = memo.Title,
                Body = memo.Content
            };
            await _mailService.SendEmailAsync(mailRequest);
            return Ok(response.Create(ResponseCode.SUCCESS, "Success", null));
        }
    }
}
