﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SmartOps_Test.DataContext;
using SmartOps_Test.Model;
using SmartOps_Test.Model.InputModel;
using SmartOps_Test.Model.ViewModel;
using SmartOps_Test.Service;
using SmartOps_Test.Service.Abstractions;
using SmartOps_Test.Utility;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartOps_Test.Controllers
{
    
    [ApiController]
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly AppDbContext _context;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly IFireBaseService _fireBaseService;
        private readonly IEmailService _mailService;
        private readonly FrontEndSettings _frontEndSettings;

        public CustomerController(AppDbContext context, UserManager<User> userManager, IMapper mapper,
            IFireBaseService fireBaseService, IEmailService mailService, IOptions<FrontEndSettings> frontEndSettings)
        {
            _userManager = userManager;
            _context = context;
            _mapper = mapper;
            _fireBaseService = fireBaseService;
            _mailService = mailService;
            _frontEndSettings = frontEndSettings.Value;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetCustomersAsync()
        {
            var response = new Response<List<CustomerViewModel>>();
            var customers = await _context.Users.Where(x => !x.IsAdmin).ToListAsync();
            if (customers.Count() > 0)
            {
                var customerResponse = _mapper.Map<List<CustomerViewModel>>(customers);
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", customerResponse));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, "Customers Record not found", null));
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync([FromBody] CustomerInputModel model)
        {
            var response = new Response<UserViewModel>();
            if (!ModelState.IsValid)
            {
                var errors = new List<string>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                    }
                }
                return BadRequest(response.Create(ResponseCode.BadRequest, errors[0], null));
            }

            var userExist = await _userManager.FindByNameAsync(model.Email);
            if (userExist != null)
            {
                return BadRequest(response.Create(ResponseCode.BadRequest, "Email already exist", null));
            }
            var contentId = await Helper.UploadAndSaveContent(_context, _fireBaseService, model.ProfilePicture,  model.Company.ToLower().Replace(" ", "_").Trim());


            var user = new User
            {
                UserName = model.Email,
                Email = model.Email,
                PhoneNumber = model.PhoneNumber,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address = model.Address,
                ContentId = contentId,
                IsAdmin = false,
                City = model.City,
                Company = model.Company,
                Country = model.Country,
                PostalCode = model.PostalCode,
                Position = model.Position,
                Summary = model.Summary
            };
            var defaultPassword = "Password1";
            var result = await _userManager.CreateAsync(user, defaultPassword);

            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, "Customer");

                var userViewModel = new UserViewModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", userViewModel));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, result.Errors.First().Description, null));
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomersByIdAsync(int id)
        {
            var response = new Response<CustomerViewModel>();
            var customer = await _context.Users.Include(x => x.Content).Where(x => !x.IsAdmin && x.Id == id).FirstOrDefaultAsync();
            if (customer != null)
            {
                var customerResponse = _mapper.Map<CustomerViewModel>(customer);
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", customerResponse));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, "Customer Record not found", null));
        }

        [AllowAnonymous]
        [HttpGet("{id}/ttl-reference/{reference}")]
        public async Task<IActionResult> GetCustomersByIdAsync(int id, string reference)
        {
            var response = new Response<CustomerViewModel>();
            var customer = await _context.Users.Include(x => x.Content).Where(x => !x.IsAdmin && x.Id == id && x.TTlReference == reference).FirstOrDefaultAsync();
            if (customer != null)
            {
                var customerResponse = _mapper.Map<CustomerViewModel>(customer);
                return Ok(response.Create(ResponseCode.SUCCESS, "Success", customerResponse));
            }
            return BadRequest(response.Create(ResponseCode.BadRequest, "Customer Record not found", null));
        }

        [Authorize]
        [HttpGet("{id}/request-receipt")]
        public async Task<IActionResult> RequestReceipt(int id)
        {
            var response = new Response<object>();
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user != null && user.TtlExpiryTime != null && DateTime.Now < user.TtlExpiryTime)
            {
                return Ok(response.Create(ResponseCode.BadRequest, "Active payment link in progress", null));
            }

            var ttlExpiryTime = DateTime.Now.AddMinutes(5);
            var ttlReference = $"ttl-{Helper.GenerateRandomString()}";
            var uploadUrl = $"{_frontEndSettings.BaseUrl}/customer-upload/{id}/{ttlReference}";

            var emailTitle = "Smartops Payment Receipt";
            var emailBody = $"Kindly click on the link below to upload your receipt as confirmation for your payment <br>" +
                $"The link is only valid for 5 mins after which your payment becomes invalidated <br>" +
                $"Having troubles, please contact customer care <p>" +
                $"payment link: {uploadUrl}";

            var mailRequest = new MailRequest
            {
                ToEmail = user.Email,
                Subject = emailTitle,
                Body = emailBody
            };

            user.TtlExpiryTime = ttlExpiryTime;
            user.TTlReference = ttlReference;
            _context.Users.Update(user);

            await _context.SaveChangesAsync();
            await _mailService.SendEmailAsync(mailRequest);
            return Ok(response.Create(ResponseCode.SUCCESS, "Payment link sent to user", null));

        }

        [AllowAnonymous]
        [HttpPost("{id}/payment-upload")]
        public async Task<IActionResult> UploadPaymentReceipt(int id, [FromBody] CustomerUploadInputModel model)
        {

            var response = new Response<object>();
            try
            {
                var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
                var contentId = await Helper.UploadAndSaveContent(_context, _fireBaseService, model.Image, $"receipt_ {id}");

                user.HasPaid = true;
                user.PaymentContentId = contentId;
                _context.Users.Update(user);
                await _context.SaveChangesAsync();

                return Ok(response.Create(ResponseCode.SUCCESS, "Success", null));
            }
            catch (Exception ex)
            {
                return BadRequest(response.Create(ResponseCode.BadRequest, "Error Occured", null));
            }
            
        }
    }
}
