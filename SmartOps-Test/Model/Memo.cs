﻿using System;
namespace SmartOps_Test.Model
{
    public class Memo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int MemoCategoryId { get; set; }
        public MemoCategory MemoCategory { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}
