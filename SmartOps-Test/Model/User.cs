﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace SmartOps_Test.Model
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsAdmin { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Company { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Position { get; set; }
        public string Summary { get; set; }
        public Content Content { get; set; }
        public int? ContentId { get; set; }
        public bool HasPaid { get; set; }
        public int? PaymentContentId { get; set; }
        public ICollection<Memo> Memos { get; set; }
        public DateTime? TtlExpiryTime { get; set; }
        public string TTlReference { get; set; }
    }
}
